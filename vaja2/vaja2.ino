#include <Ticker.h>
#include <Wire.h>

#define PIN_SCL 14
#define PIN_SDA 12


char shift_bits(char state){
    if(state & 0b00001000){
        state = state << 1;
        state &=0x0f;
        state = state | 1;
    }else{
        state = state << 1;
        state &=0x0f;
    }
    return state;
}

char init_no_led = 1;
char state = 1;
int speed = 1000;

Ticker ticker;
Ticker ticker1;
void change_state(){
    char new_state = shift_bits(state);
    state = new_state;
    new_state = ~new_state;
    Wire.beginTransmission(56);
    Wire.write(new_state);
    Wire.endTransmission();
    // write new state
}

void read_buttons_and_act(){
  Wire.requestFrom(56, 1);
        uint8_t buttons = Wire.read();
        uint8_t b0 = !(buttons & 0b00010000);
        uint8_t b1 = !(buttons & 0b00100000);
        uint8_t b2 = !(buttons & 0b01000000);
        uint8_t b3 = !(buttons & 0b10000000);
        // spremenit hitrost
        if(b2){
          ticker.detach();
          speed += 100; // upočasni
          ticker.attach_ms(speed, change_state);
        }else if(b3){
          ticker.detach();
          if(speed > 100)  speed -= 100;
          ticker.attach_ms(speed, change_state);
        }
        // spremenit število ledic
        // zmanjšvanje
        else if(b0){
          if(!(init_no_led <= 1)){
              ticker.detach();
              init_no_led--;              
              Serial.println(init_no_led,DEC);
              state = 0;
              for(char i=0; i<init_no_led; i++){
                  state = state << 1;
                  state |= 1;
              }
              ticker.attach_ms(speed, change_state); 
          }
        }else if(b1){
          if(!(init_no_led >= 3)){
              ticker.detach();
              init_no_led++;
              Serial.println(init_no_led,DEC);
              state = 0;
              for(char i=0; i<init_no_led; i++){
                  state = state << 1;
                  state |= 1;
              }
              ticker.attach_ms(speed, change_state);     
          }
        }
}

void setup(){
    // setup i2c
    Serial.begin(115200);
    Wire.begin(PIN_SDA, PIN_SCL);
    Wire.setClock(100000);
    ticker.attach_ms(speed, change_state);
    ticker1.attach_ms(250, read_buttons_and_act);
    
}

void loop(){
  delay(1);
}



