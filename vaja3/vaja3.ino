#include <Ticker.h>
#include <Wire.h>

#define PIN_SCL 14
#define PIN_SDA 12
#define MPU_925_ADDR 0b1101000

int16_t read_2B_from_register(uint8_t ADDR_DEV, uint8_t ADDR_REG){
    Wire.beginTransmission(ADDR_DEV);
    Wire.write(ADDR_REG);
    Wire.endTransmission();

    Wire.requestFrom(ADDR_DEV, 2);
    uint16_t ret = Wire.read();
    ret = ret << 8;
    ret |= Wire.read();
    return ret;
}

Ticker ticker;
Ticker ticker1;
Ticker ticker2;


float x_deg = 0,y_deg = 0,z_deg = 0;

//int16_t x=read_2B_from_register(MPU_925_ADDR,0x43);
//int16_t y=read_2B_from_register(MPU_925_ADDR,0x45);
//int16_t z=read_2B_from_register(MPU_925_ADDR,0x47);


void read_data_from_gyro(){
    static uint8_t x_count = 0;
    static uint8_t y_count = 0;
    static uint8_t z_count = 0;
   int16_t x; 
   int16_t y; 
   int16_t z;
    //for(int i = 0; i<10 ;i++){
        x = read_2B_from_register(MPU_925_ADDR,0x43);//131.0;
        y = read_2B_from_register(MPU_925_ADDR,0x45);//131.0;
        z = read_2B_from_register(MPU_925_ADDR,0x47);//131.0;
    //}
    float x_val = (x - 25)/131.0;
    float y_val = (y + 20)/131.0;
    float z_val = (z - 30)/131.0;

    x_deg += x_val*0.1;
    x_deg = fmod(x_deg,360);
    if(x_deg < 0) x_deg+=360;
    y_deg += y_val*0.1;
    y_deg = fmod(y_deg,360);
    if(y_deg < 0) y_deg+=360;
    z_deg += z_val*0.1;
    z_deg = fmod(z_deg,360);
    if(z_deg < 0) z_deg+=360;

    Serial.print("x: ");
    Serial.print(x_deg, DEC);
    Serial.print(" y: ");
    Serial.print(y_deg, DEC);
    Serial.print(" z: ");
    Serial.println(z_deg, DEC);    
}


void set_leds(){
    float z=z_deg;

    uint8_t state;

    if(z<=89){
        state=0x01;
    }else if(z<=179){
        state=0x03;
    }else if(z<=269){
        state=0x07;
    }else{
        state=0x0f;
    }
    state = ~state;

    Wire.beginTransmission(56);
    Wire.write(state);
    Wire.endTransmission();
}

void read_temperature(){
    uint16_t raw_temp = read_2B_from_register(MPU_925_ADDR, 65);
    float temp;

    temp = (float) raw_temp / 333.87 + 21.0;

    Serial.print("TEMP: ");
    Serial.println(temp, DEC);
}


void setup(){
    // setup i2c
    Serial.begin(115200);
    Wire.begin(PIN_SDA, PIN_SCL);
    Wire.setClock(100000);
    ticker.attach_ms(100, read_data_from_gyro);
    ticker1.attach_ms(50, set_leds);
    ticker2.attach_ms(500, read_temperature);
    
}

void loop(){
  delay(250);
  
    
}







