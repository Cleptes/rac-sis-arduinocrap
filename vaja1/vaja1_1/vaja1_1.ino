#define LED_PIN 2
#define LED_ON 0
#define LED_OFF 1

void setup(){
    pinMode(LED_PIN, OUTPUT);
}


static unsigned long old_millis = millis();
void loop(){
    if(digitalRead(LED_PIN)){
        if(millis()-old_millis >= 700){
            old_millis=millis();        
            digitalWrite(LED_PIN,0);
        }
    }else{
        if(millis()-old_millis >= 300){
            old_millis=millis();        
            digitalWrite(LED_PIN,1);
        }
    }
}

