

#define LED_PIN 2
#define LED_ON 0
#define LED_OFF 1

void setup(){
    pinMode(LED_PIN, OUTPUT);
}

void loop(){
    digitalWrite(LED_PIN,LED_ON);
    delay(700);
    digitalWrite(LED_PIN,LED_OFF);
    delay(300);    
}

