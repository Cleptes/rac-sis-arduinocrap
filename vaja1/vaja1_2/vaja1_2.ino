#include <Ticker.h>

#define LED_PIN 2
#define LED_ON 0
#define LED_OFF 1

Ticker tickOn;
Ticker tickOff;

void setup(){
    pinMode(LED_PIN, OUTPUT);
    tickOn.attach(0.3, writePin, 1);
    Serial.begin(115200);
}

void loop(){
    delay(1000);
    Serial.write("Test\n");
}

void writePin(int val){
    if(val){
        digitalWrite(LED_PIN,val);
        tickOn.detach();
        tickOff.attach(0.7, writePin, 0);
    }else{
        digitalWrite(LED_PIN,val);
        tickOff.detach();
        tickOn.attach(0.3, writePin, 1);
    }
}

