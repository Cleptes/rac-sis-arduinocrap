#include <Ticker.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Wire.h>

// zamenjajte uporabnika in geslo:
#define USERNAME "admin"
#define PASSWORD "admin"

#define WIFI_STA_SSID "cleptes_hotspot"
#define WIFI_STA_PASSWORD "testtest1"
#define WIFI_AP_SSID "cleptes"
#define WIFI_AP_PASSWORD "password123"


// inicializirajte server na portu 80:
ESP8266WebServer server(80);
Ticker ticker2;

// globalne spremenljivke
#define MODE 0
#define PIN_LED 2
#define PIN_SCL 14
#define PIN_SDA 12
#define TEMP_LOG_SIZE 50

#define MPU_925_ADDR 0b1101000

int16_t read_2B_from_register(uint8_t ADDR_DEV, uint8_t ADDR_REG){
    Wire.beginTransmission(ADDR_DEV);
    Wire.write(ADDR_REG);
    Wire.endTransmission();

    Wire.requestFrom(ADDR_DEV, 2);
    uint16_t ret = Wire.read();
    ret = ret << 8;
    ret |= Wire.read();
    return ret;
}


char read_temperature(){
    int16_t raw_temp = read_2B_from_register(MPU_925_ADDR, 65);
    int8_t temp_c;

    temp_c = (raw_temp*1000) / 33387 ;
    float temp = (float)temp_c/10.0 + 21.0;
    Serial.print("TEMP: ");
    Serial.println(temp, DEC);
    return temp_c;
}

int8_t temp_log[TEMP_LOG_SIZE] = {0};

void log_temp(){
  static char pointer = 0;

  if(pointer==0){
    temp_log[pointer]=read_temperature();
  }else{
    temp_log[pointer]=read_temperature()-temp_log[0];
  }
  
  for(int i=0;i<50;i++){
    Serial.print(temp_log[i],DEC);
    Serial.print(", ");
  }
  Serial.println();
  
  pointer=(pointer+1)%50; 
}

void handleTemp(){
  if (!is_authentified()){
      server.sendHeader("Location","/login");
      server.sendHeader("Cache-Control","no-cache");
      server.send(301);
      return;
    }
    char temp[400];
    int sec = millis() / 1000;
    int min = sec / 60;
    int hr = min / 60;
    snprintf ( temp, 400,
    "<html>\
    <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>ESP8266 Demo</title>\
    <style>\
    body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
    </head>\
    <body>\
    <h1>Temperatura</h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
    <img src=\"/test.svg\" />\
     <p> If u wanna go home go <a href=\"/\">HERE</a></p>\
    </body>\
    </html>",
    hr, min % 60, sec % 60
    );
    server.send ( 200, "text/html", temp );
}

void handleSvg(){

    if (!is_authentified()){
      server.sendHeader("Location","/login");
      server.sendHeader("Cache-Control","no-cache");
      server.send(301);
      return;
    }

    String out = "";
    char temp[100];
    out += "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"500\" height=\"150\">\n";
    out += "<rect width=\"500\" height=\"150\" fill=\"rgb(250, 230, 210)\" stroke-width=\"1\" stroke=\"rgb(0, 0, 0)\" />\n";
    out += "<g stroke=\"black\">\n";
    int temp_1, temp_2;
    temp_1 = temp_log[0]+210;
    temp_1=150-(temp_1-200)*1.5;

    temp_2 = temp_log[0]+temp_log[1]+210;
    temp_1=150-(temp_1-200)*1.5;

    sprintf(temp, "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke-width=\"1\" />\n", 0, temp_1, 10, temp_2);
    out += temp;       
    for (int x = 1; x < TEMP_LOG_SIZE; x++) {
        if(x+1<TEMP_LOG_SIZE){
            temp_1=temp_log[0]+temp_log[x]+210;
            temp_1=150-(temp_1-200)*1.5;
            temp_2=temp_log[0]+temp_log[x+1]+210;
            temp_2=150-(temp_2-200)*1.5;

            sprintf(temp, "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke-width=\"1\" />\n", x*10, temp_1, (x+1)*10, temp_2);
            out += temp;       
        }
    }
    out += "</g>\n</svg>\n";
    server.send ( 200, "image/svg+xml", out);
}
//Check if header is present and correct
bool is_authentified(){
  Serial.println("Enter is_authentified");
  if (server.hasHeader("Cookie")){
    Serial.print("Found cookie: ");
    String cookie = server.header("Cookie");
    Serial.println(cookie);
    if (cookie.indexOf("ESPSESSIONID=1") != -1) {
      Serial.println("Authentification Successful");
      return true;
    }
  }
  Serial.println("Authentification Failed");
  return false;
}


//login page, also called for disconnect
void handleLogin(){
  String msg;
  if (server.hasHeader("Cookie")){
    Serial.print("Found cookie: ");
    String cookie = server.header("Cookie");
    Serial.println(cookie);
  }
  if (server.hasArg("DISCONNECT")){
    Serial.println("Disconnection");
    server.sendHeader("Location","/login");
    server.sendHeader("Cache-Control","no-cache");
    server.sendHeader("Set-Cookie","ESPSESSIONID=0");
    server.send(301);
    return;
  }
  if (server.hasArg("USERNAME") && server.hasArg("PASSWORD")){
    if (server.arg("USERNAME") == USERNAME &&  server.arg("PASSWORD") == PASSWORD ){
      server.sendHeader("Location","/");
      server.sendHeader("Cache-Control","no-cache");
      server.sendHeader("Set-Cookie","ESPSESSIONID=1");
      server.send(301);
      Serial.println("Log in Successful");
      return;
    }
  msg = "Wrong username/password! try again.";
  Serial.println("Log in Failed");
  }
  String content = "<html><body><form action='/login' method='POST'>To log in enter user id and password:<br>";
  content += "User:<input type='text' name='USERNAME' placeholder='user name'><br>";
  content += "Password:<input type='password' name='PASSWORD' placeholder='password'><br>";
  content += "<input type='submit' name='SUBMIT' value='Submit'></form>" + msg + "<br>";
  content += "You also can go <a href='/inline'>here</a></body></html>";
  server.send(200, "text/html", content);
}

//root page can be accessed only if authentification is ok
void handleRoot(){
  Serial.println("Enter handleRoot");
  String header;
  if (!is_authentified()){
    server.sendHeader("Location","/login");
    server.sendHeader("Cache-Control","no-cache");
    server.send(301);
    return;
  }
  String content = "<html><body><H2>hello, you successfully connected to esp8266!</H2><br>";
  content += "You can access this page until you <a href=\"/login?DISCONNECT=YES\">disconnect</a><br><hr />";

 
  content+="<form action=\"/led\"> <input type=\"submit\" value=\"";
  if(!digitalRead(2)){  content+="Ugasni LED";}
  else {content+="Prizgi LED"; }
  content+="\" />  </form>";
  content+="<p> If u wanna get some temp go <a href=\"/temp\">HERE</a></p></body></html>";
  
  server.send(200, "text/html", content);
}


void handleLED(){
  
   if (!is_authentified()){
    server.sendHeader("Location","/login");
    server.sendHeader("Cache-Control","no-cache");
    server.send(301);
    return;
  }

  digitalWrite(PIN_LED, !digitalRead(PIN_LED));
  
  server.sendHeader("Location","/");
  server.sendHeader("Cache-Control","no-cache");
  server.send(301);  

  
  


}


//no need authentification
void handleNotFound(){
 
  String vsebina = "<html><head></head><body><h1>404 not found<h1><p> <a href=\"/\">RETURN</a></body>"; // dopišite ustrezno HTML kodo
  server.send(404, "text/html", vsebina);
}

void setupWiFiAP(){
  WiFi.mode(WIFI_AP);
  WiFi.softAP(WIFI_AP_SSID, WIFI_AP_PASSWORD);
}

void setupWiFiSTA(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_STA_SSID, WIFI_STA_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }
  Serial.print("\nIP:");
  Serial.println(WiFi.localIP());
}

void setup() {
  // put your setup code here, to run once:

  /*
   * Dodajte ustrezno kodo za inicializacijo.
   */
  
  
  Serial.begin(115200);
  pinMode(PIN_LED, OUTPUT);
#if MODE == 0
  setupWiFiAP();
#else if MODE == 1
  setupWiFiSTA();
#endif
Wire.begin(PIN_SDA, PIN_SCL);
    Wire.setClock(100000);
ticker2.attach_ms(1000, log_temp);

  //**** nastavitve strežnika:
  server.on("/", handleRoot);
  server.on("/login", handleLogin);
  server.on("/led", handleLED);
  server.on("/inline", [](){
    server.send(200, "text/plain", "this works without need of authentification");
  });
  server.on("/test.svg",handleSvg);
  server.on("/temp",handleTemp);
  server.onNotFound(handleNotFound);
  
  //here the list of headers to be recorded
  const char * headerkeys[] = {"User-Agent","Cookie"} ;
  size_t headerkeyssize = sizeof(headerkeys)/sizeof(char*);
  //ask server to track these headers
  server.collectHeaders(headerkeys, headerkeyssize );
  
  // zaženemo strežnik:
  server.begin();

  Serial.println("HTTP server started");
}

/* ----------------------------------------------------------------
 * loop:
 */
void loop() {
  // put your main code here, to run repeatedly:
  server.handleClient();
}










